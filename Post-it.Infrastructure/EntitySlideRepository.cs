﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_it.Domain.Common;

namespace Post_it.Infrastructure
{
    public class EntitySlideRepository : ISlideRepository
    {
        private ApplicationContext db;
        public EntitySlideRepository()
        {
            db = new ApplicationContext();
        }

        public IEnumerable<Slide> GetAll()
        {
            return db.Slides;
        }

        public void Save(Slide _slide)
        {
            db.Slides.Add(_slide);
            db.SaveChanges();
        }

        public void Update(Slide _slide)
        {
            throw new NotImplementedException();
        }
        
        public void Delete(Slide _slide)
        {
            db.Slides.Remove(_slide);
            db.SaveChanges();
        }

        public Slide GetById(int id)
        {
            return db.Slides.Find(id);
        }

        public Slide FindByPath(string _path)
        {
            throw new NotImplementedException();
        }
    }
}
