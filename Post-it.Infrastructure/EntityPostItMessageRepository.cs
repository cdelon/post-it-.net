﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_it.Domain.Common;

namespace Post_it.Infrastructure
{
    public class EntityPostItMessageRepository : IPostItMessageRepository
    {
        private ApplicationContext db;
        public EntityPostItMessageRepository()
        {
            db = new ApplicationContext();
            if (!initialized)
                init();
        }

        private static bool initialized;

        //A voir ce qu'on fait dedans ... Si on crée le compte admin lors du premier lancement ... ou si c'est inutile
        private void init()
        {
            if (!db.PostIts.Any())
            {
                db.PostIts.Add(new PostItMessage { ID = 100, Message="Default message Post It", Author="DefaultUser" });
                db.SaveChanges();
            }
            initialized = true;
        }

        public IEnumerable<PostItMessage> GetAll()
        {
            return db.PostIts;
        }

        public void Save(PostItMessage _postIt)
        {
            db.PostIts.Add(_postIt);
            db.SaveChanges();
        }

        public void Update(PostItMessage _user)
        {
            throw new NotImplementedException();
        }

        public void Delete(PostItMessage _postIt)
        {
            db.PostIts.Remove(_postIt);
            db.SaveChanges();
        }

        public PostItMessage GetByMessage(string message)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PostItMessage> GetByAuthor(string author)
        {
            //throw new NotImplementedException();
            return null;
        }

        public PostItMessage GetById(int id)
        {
            return db.PostIts.Find(id);
        }
    }
}