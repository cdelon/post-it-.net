﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_it.Domain.Common;

/*
 * Implementation du Context de la base de donnée
 */

namespace Post_it.Infrastructure
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Slide> Slides { get; set; }
        public DbSet<PostItMessage> PostIts { get; set; }
        
        public ApplicationContext () : base ("PostIt")
	    {

	    }
    }
}
