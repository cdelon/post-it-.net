#Post-it

##Presentation
---------------------------
###Language: 
C# ASP.Net MVC 4.5
###Description:
The purpose of this website is to help people who makes live presentations, and can receive live comments by the audience.

In this website there is three differents Views:
*   _Presentator_ : For the person who makes the show, in this case you can create a presentation with a simple editor, you can also display your presentation to the audience and receive messages (post-it) sendend by the audience
*   _Simple_User_ : For the person no-logged, he just can attend to the presentation
*   _User_Logged_ : For the person logged with an account, he can attend to the presentation and send message (post-it) to the Presentator

##Build informations
You must have Microsoft Visual Studio 2012 and ASP.net 4.5 on your computer to build this

##Authors
* DELON Christophe
* COTTIGNIES Renaud
* DE FROBERVILLE Jean-Baptiste
* COMBETTE Tristan

