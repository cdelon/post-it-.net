﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_it.Domain.Common;

namespace Post_it.BootStraper
{
    public class SlideRepositoryFactory
    {
        private static readonly SlideRepositoryFactory current = new SlideRepositoryFactory();
        public static SlideRepositoryFactory Current
        {
            get
            {
                return current;
            }
        }

        private Func<ISlideRepository> definition;
        public void SetRepository(Func<ISlideRepository> implementation)
        {
            definition = implementation;
        }
        public ISlideRepository GetSlideRepository()
        {
            return definition();
        }
    }
}
