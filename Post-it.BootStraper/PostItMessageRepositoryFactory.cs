﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_it.Domain.Common;

namespace Post_it.BootStraper
{
    public class PostItMessageRepositoryFactory
    {
        private static readonly PostItMessageRepositoryFactory current = new PostItMessageRepositoryFactory();
        public static PostItMessageRepositoryFactory Current
        {
            get
            {
                return current;
            }
        }

        private Func<IPostItMessageRepository> definition;
        public void SetRepository(Func<IPostItMessageRepository> implementation)
        {
            definition = implementation;
        }
        public IPostItMessageRepository GetPostItMessageRepository()
        {
            return definition();
        }
    }
}
