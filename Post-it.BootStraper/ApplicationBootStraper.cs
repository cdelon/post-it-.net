﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Post_it.Infrastructure;

namespace Post_it.BootStraper
{
    public class ApplicationBootStraper
    {
        public void StartUp()
        {
            //initialisation de la base de donnée en liant avec les entityRepository 
            Database.SetInitializer<ApplicationContext>(new DropCreateDatabaseIfModelChanges<ApplicationContext>());
            PostItMessageRepositoryFactory.Current
                .SetRepository(() => new EntityPostItMessageRepository()); //On set le singleton UserRepositoryFactory avec notre EntityRepo
            SlideRepositoryFactory.Current
                .SetRepository(() => new EntitySlideRepository()); //On set le singleton UserRepositoryFactory avec notre EntityRepo
        }
    }
}
