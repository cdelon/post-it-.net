﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Library_XML
{
    public class Slides
    {
        public List<String> SlidesList {get;set;}
        public String title {get;set;}
        public Slides()
        {

            SlidesList = new List<String>();
        }
    }

    //public class Slide
    //{
    //    [XmlElement("value")]
    //     public String slideValue;
    //}
}
