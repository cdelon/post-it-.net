﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;


// Classe de log
namespace Library_XML
{
    class ErrorLog
    {
        private StreamWriter log;

        public ErrorLog()
        {
            var logfileName = "logfile" + DateTime.Now.ToString("_yyyy-MM-dd") + ".txt";
            var rootpath = AppDomain.CurrentDomain.BaseDirectory;
            logfileName = Path.Combine(rootpath, logfileName);
            if (!File.Exists(logfileName))
            {
                log = new StreamWriter(logfileName);
            }
            else
            {
                log = File.AppendText(logfileName);
            } 
        }

        public void Error(string message, string module)
        {
            WriteEntry(message, "Error", module);
        }

        public void Error(Exception ex, string module)
        {
            WriteEntry(ex.Message, "Error", module);
        }

        public void Warning(string message, string module)
        {
            WriteEntry(message, "Warning", module);
        }

        public void Info(string message, string module)
        {
            WriteEntry(message, "Info", module);
        }

        private void WriteEntry(string message, string type, string module)
        {
            log.WriteLine(
                    string.Format("{0} ->  {1}:  {2}, {3}",
                                  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                  type,
                                  module,
                                  message));
            log.Close();
        }
    }
}
