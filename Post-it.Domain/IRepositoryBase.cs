﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/**
 * Fonctions retrouvé dans toute les Repository et permet de récupéré une base
 */

namespace Post_it.Domain
{
    public interface IRepositoryBase<T>
    {
        void Save(T item);
        void Delete(T item);
        void Update(T item);
        T GetById(int Id);
        IEnumerable<T> GetAll();
        //IEnumerable<T> Get(Func<T, bool> filter);
    }
}
