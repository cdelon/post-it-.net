﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_it.Domain.Common
{
    public interface IPostItMessageRepository : IRepositoryBase<PostItMessage>
    {
        PostItMessage GetByMessage(string message);
        IEnumerable<PostItMessage> GetByAuthor(string author);
    }
}
