﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_it.Domain.Common
{
    public class PostItMessage
    {
        public int ID { get; set; }
        public string Message { get; set; }
        public string Author { get; set; }

        public PostItMessage() { }

        public PostItMessage(string _message, string _author)
        {
            Message = _message;
            Author = _author;
        }
    }
}
