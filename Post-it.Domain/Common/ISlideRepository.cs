﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Fonction propre au SlideRepository
 * Permettra de grérer les donnée des objets Slide
 */

namespace Post_it.Domain.Common
{
    public interface ISlideRepository : IRepositoryBase<Slide>
    {
        Slide FindByPath(string path);
    }
}
