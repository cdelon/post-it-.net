﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Contient les donnée de user pour la base de donnée
 **/

namespace Post_it.Domain.Common
{
    public class Slide
    {
        public int ID { get; set; }
        public string JSONData { get; set; }

        public Slide() { }

        public Slide(string _chemin)
        {
            JSONData = _chemin;
        }
    }
}
