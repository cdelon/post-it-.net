﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Post_it.Models
{
        public class SlideModel
        {
            [Required]
            [Display(Name = "Save Path")]
            public string slideFilePath { get; set; }
        }

        public class PostItModel
        {
            [Required]
            [Display(Name = "PostIt message")]
            public string PostItMessage { get; set; }

            [Required]
            [Display(Name = "Author")]
            public string Author { get; set; }
        }
}