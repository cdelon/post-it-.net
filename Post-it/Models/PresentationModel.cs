﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Post_it.Models
{
    public class PresentationModel
    {
        public String Title { get; set; }
        public List<String> listOfSlides { get; set; }
    }
}