﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Post_it.Domain.Common;
using Post_it.BootStraper;
using Post_it.Models;

namespace Post_it.Controllers
{
    public class AdminController : Controller
    {
        private IPostItMessageRepository postIt_rep;
        private ISlideRepository slide_rep;

        public AdminController()
	    {
            postIt_rep = PostItMessageRepositoryFactory.Current.GetPostItMessageRepository();
            slide_rep = SlideRepositoryFactory.Current.GetSlideRepository();
	    }

        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        public ActionResult PostItMessage()
        {
            return View(postIt_rep.GetAll().ToList());
        }

        public ActionResult Slides()
        {
            return View(slide_rep.GetAll().ToList());
        }


        public ActionResult CreateSlide()
        {
            return View();
        }

        public ActionResult CreatePostIt()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePostIt(PostItModel model, string returnUrl)
        {
            postIt_rep.Save(new PostItMessage(model.PostItMessage, model.Author));
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSlide(SlideModel model, string returnUrl)
        {
            slide_rep.Save(new Slide(model.slideFilePath));
            return View(model);
        }

        //// GET: /AdminControler/Slides/3

        public ActionResult DeleteSlide(int id)
        {
            Slide slide = slide_rep.GetById(id);
            if (slide == null)
            {
                return HttpNotFound();
            }
            return View(slide);
        }

        public ActionResult DeletePostIt(int id)
        {
            PostItMessage postIt = postIt_rep.GetById(id);
            if (postIt == null)
            {
                return HttpNotFound();
            }
            return View(postIt);
        }

        [HttpPost, ActionName("DeletePostIt")]
        public ActionResult DeleteConfirmedPostIt(int id)
        {
            PostItMessage postIt = postIt_rep.GetById(id);
            if (postIt == null)
            {
                return HttpNotFound();
            }
            else
            {
                postIt_rep.Delete(postIt);
            }
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("DeleteSlide")]
        public ActionResult DeleteConfirmedSlide(int id)
        {
            Slide slide = slide_rep.GetById(id);
            if (slide == null)
            {
                return HttpNotFound();
            }
            else
            {
                slide_rep.Delete(slide);
            }
            return RedirectToAction("Index");
        }

    }
}
