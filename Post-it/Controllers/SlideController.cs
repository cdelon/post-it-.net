﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Post_it.BootStraper;
using Post_it.Domain.Common;

namespace Post_it.Controllers
{
    public class SlideController : Controller
    {
        //
        // GET: /Slide/
        private ISlideRepository slide_rep;
        public ActionResult Index()
        {
            return View();
        }

     

        public ActionResult GetNextSlide(int num, String projectname)
        {
            String result = null;
            slide_rep = SlideRepositoryFactory.Current.GetSlideRepository();
            Slide myprez = slide_rep.GetById(int.Parse(projectname));
            JavaScriptSerializer serialiser = new JavaScriptSerializer();
            Slides np1 = serialiser.Deserialize<Slides>(myprez.JSONData);
            if (num < np1.SlidesList.Count-1)
            {
                result = np1.SlidesList[num+1];
            }
           
            return Json(result, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetPreviousSlide(int num, String projectname)
        {
            String result = null;
            slide_rep = SlideRepositoryFactory.Current.GetSlideRepository();
            Slide myprez = slide_rep.GetById(int.Parse(projectname));
            JavaScriptSerializer serialiser = new JavaScriptSerializer();
            Slides np1 = serialiser.Deserialize<Slides>(myprez.JSONData);
            if (num > 0)
            {
               result = np1.SlidesList[num-1];
            }

            return Json(result, JsonRequestBehavior.DenyGet);
        }
    }
}
