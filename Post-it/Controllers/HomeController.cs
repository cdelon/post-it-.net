﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Post_it.BootStraper;
using Post_it.Domain.Common;

namespace Post_it.Controllers
{
    public class HomeController : Controller
    {
        private ISlideRepository slide_rep;

        public HomeController()
        {
            slide_rep = SlideRepositoryFactory.Current.GetSlideRepository();
        }
        public ActionResult Index()
        {
            ViewBag.Message = "Modifiez ce modèle pour dynamiser votre application ASP.NET MVC.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Votre page de description d’application.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Votre page de contact.";

            return View();
        }

        public ActionResult Slide()
        {
            return View(slide_rep.GetAll().ToList());
        }

        public ActionResult DropDownListSlides()
        {
            return View();
        }

        //Acced à la page d'administration de l'application
        public ActionResult Admin()
        {
            if (WebSecurity.CurrentUserName.Equals("admin"))
            {
                return View();
            }
            else
            {
                return View("_NotAuthorized");
            }
        }

        public ActionResult SlideEditor()
        {
            ViewBag.Message = "Slide Editor";
            return View();
        }

        public ActionResult SendPostIt()
        {
            return View();
        }
    }
}
