﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Post_it.Domain.Common;
using Post_it.BootStraper;
using System.Web.Script.Serialization;

namespace Post_it.Controllers
{
    public class SlideEditorController : Controller
    {
        //
        // GET: /SlideEditor/
        private ISlideRepository slide_rep;
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(String title, List<String> slides)
        {
            slide_rep = SlideRepositoryFactory.Current.GetSlideRepository();
            Slides np = new Slides();
            np.title = title;
            np.SlidesList = slides;

            JavaScriptSerializer serialiser = new JavaScriptSerializer();

            String monJSON = serialiser.Serialize(np);

            slide_rep.Save(new  Slide(monJSON));

//Slides np1 = serialiser.Deserialize<Slides>(monJSON);

            //var test = o.ToString();
           /// slide_rep = SlideRepositoryFactory.Current.GetSlideRepository();
            //Console.WriteLine(slides);
            // Appel de la classe de serialisation afin d'enregistrer les slide dans un fichier qui aura pour nom le titre du slide
          // Library_XML.XmlSlideSerializer xmlS = new Library_XML.XmlSlideSerializer();
          // xmlS.AddSlideProject(slides, title);

            // Enregistrement de l'url dans la bdd
          // slide_rep.Save(new Slide(xmlS.getUrl()));

            return Json("Presentation Saved", JsonRequestBehavior.DenyGet);
        }
    }

    public class Slides
    {
        public List<String> SlidesList { get; set; }
        public String title { get; set; }
        public Slides()
        {
            SlidesList = new List<String>();
        }
    }
}
