﻿using System;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Post_it.BootStraper;
using Post_it.Domain.Common;


public class ChatHub : Hub
{
    private IPostItMessageRepository postIt_rep;
    public Task Admingroup()
    {
        return Groups.Add(Context.ConnectionId, "admin");

    }

    public void Sendslide(string name, string message)
    {
        Clients.All.broadcastMessage(name, message);   // Envoi a toute les personnes connectés
    }

    public void Sendpostit(string name, string message)
    { 
        Clients.Group("admin").broadcastPostIt(name, message);   // Envoi seulement a l'admin
        postIt_rep = PostItMessageRepositoryFactory.Current.GetPostItMessageRepository();
        postIt_rep.Save(new PostItMessage(message, name));
    }
}
